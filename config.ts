import { parseArgs } from "@std/cli";
import "@std/dotenv/load";

const env = Deno.env.toObject();
const args = parseArgs(Deno.args, { string: ["user"] });
if (!args.user) args.user = env.SUBSTITUTE_USER;

export const APP_NAME = "Derive";
export const APP_VER = "v1.9.0";
export const USERNAME = args.user
  ? args.user.toLowerCase()
  : env.USERNAME.toLowerCase() || "default";
export const APP_HOST = env.APP_HOST || "localhost";
export const APP_PORT = env.APP_PORT || 4000;

export const LOG_FILE_PATH = env.LOG_FILE_PATH || "derive.log.txt";
