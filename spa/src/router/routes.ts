import { RouteRecordRaw } from "vue-router";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: () => import("../layouts/Default.vue"),
    children: [
      { path: "", redirect: () => "/conversions" },
      {
        path: "index",
        component: () => import("../pages/Index.vue"),
        name: "home",
      },
      { path: "types", component: () => import("../pages/Types.vue") },
      { path: "approvals", component: () => import("../pages/Approvals.vue") },
      { path: "viewer", component: () => import("../pages/Viewer.vue") },
      { path: "translate", component: () => import("../pages/Translate.vue") },
      {
        path: "conversions",
        component: () => import("../pages/Conversions.vue"),
      },
      // { path: 'conversions/print/:vin', component: () => import('../pages/FetchPrint.vue') },
      { path: "vin", component: () => import("../pages/Vin.vue") },
      { path: "tenders", component: () => import("../pages/Tenders.vue") },
      { path: "marketing", component: () => import("../pages/Marketing.vue") },
      { path: "its", component: () => import("../pages/Catalog.vue") },
      { path: "label", component: () => import("../pages/Label.vue") },
      { path: "dealers", component: () => import("../pages/Dealers.vue") },
      { path: "utils", component: () => import("../pages/Utils.vue") },
    ],
  },
  {
    path: "/login",
    component: () => import("../layouts/NoAuth.vue"),
    children: [
      // { path: '', component: () => import('../pages/Login.vue') }
    ],
  },
  {
    path: "/share",
    component: () => import("../layouts/NoAuth.vue"),
    children: [],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("../pages/ErrorNotFound.vue"),
  },
];

export default routes;
