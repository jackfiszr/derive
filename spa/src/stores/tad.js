import brands from "../utils/brands.json" with { type: "json" };
import types from "../utils/types.json" with { type: "json" };
import { useNavStore } from "./nav.js";
import { defineStore } from "pinia";
// import { parseString } from "xml2js";

const nav = useNavStore();

export const useTadStore = defineStore("tad", {
  state: () => {
    return {
      brands,
      types,
      approvals: {},
    };
  },
  getters: {
    loadedApprovals: (state) => Object.keys(state.approvals),
    approvalBySelection: (state) => {
      const key = nav.selectedXml;
      return state.approvals[key];
    },
    approvalHeadBySelection: (state) => {
      const approval = state.approvalBySelection;
      if (approval && "Kopf" in approval) {
        return approval.Kopf;
      }
    },
    approvalBodyBySelection: (state) => {
      const approval = state.approvalBySelection;
      if (approval && "Saetze" in approval) {
        return approval.Saetze.M1vollst;
      }
    },
    variantVersionData: (state) => {
      const selectedApproval = state.approvalBySelection;
      const selectedVariant = nav.selectedVariant;
      const selectedVersion = nav.selectedVersion;
      if (selectedApproval && "Saetze" in selectedApproval) {
        return selectedApproval.Saetze.M1vollst.filter((el) =>
          el["_0.2.VARIANTE"] === selectedVariant
        ).find((el) => el["_0.2.VERSION"] === selectedVersion);
      }
      return undefined;
    },
  },
  actions: {
    async fetchApproval(xmlId) {
      if (xmlId in this.approvals) {
        return;
      }
      /*const response = await fetch(`data/${xmlId}.xml`);
      const xml = await response.text();
      parseString(xml, (err, json) => {
        if (err) {
          alert(err);
        }
        const payload = {};
        payload[xmlId] = Object.values(json)[0];
        this.approvals = { ...this.approvals, ...payload };
      });*/
      try {
        const resp = await fetch(`api/xml/${xmlId}`);
        if (resp.ok) {
          const json = await resp.json();
          this.approvals = {
            ...this.approvals,
            [xmlId]: json["KlasseM1vollstKBA"]
              ? json["KlasseM1vollstKBA"]
              : json["KlasseM1vollstHerst"],
          };
          if (!Array.isArray(this.approvals[xmlId].Saetze.M1vollst)) {
            this.approvals[xmlId].Saetze.M1vollst = [
              this.approvals[xmlId].Saetze.M1vollst,
            ];
          }
          console.log(`${xmlId}.xml loaded`);
        } else {
          console.log(`${xmlId}.xml not found`);
        }
      } catch (err) {
        console.error(err);
      }
    },
  },
});
