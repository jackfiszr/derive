import { defineStore } from "pinia";

export const useCnvStore = defineStore("cnv", {
  state: () => {
    return {
      driveDataStr: "",
      modifications: null,
      rapidList2016: [],
      rapidList2018: [],
      rapidList2019: [],
      fabiaList: [],
      scalaList: [],
      ibizaList: [],
      // nadinList: [],
      ajaxErrors: [],
      loadStatus: {},
    };
  },
  getters: {
    driveDataJson: (state) => {
      if (state.driveDataStr === "") {
        return;
      }
      try {
        return JSON.parse(state.driveDataStr);
      } catch (e) {
        console.error("[driveDataJson getter]: ", e);
      }
      return {};
    },
    printedDocumentsList: (state) => {
      return state.rapidList2016.concat(
        state.rapidList2018,
        state.rapidList2018,
        state.fabiaList,
        state.scalaList,
        state.ibizaList,
      );
    },
    vinLoadStatus: (state) => (vin) => {
      return state.loadStatus[vin];
    },
  },
  actions: {
    async fetchModifications() {
      try {
        const resp = await fetch("/api/modifications");
        const data = await resp.json();
        this.modifications = data;
      } catch (err) {
        console.error(err);
      }
    },
    fetchPrintedDocsLists(listsArr) {
      listsArr.forEach(async (list) => {
        const apiUrl = list.replace("List", "");
        try {
          const resp = await fetch(`/api/${apiUrl}`);
          const data = await resp.json();
          this[list] = data.reverse();
        } catch (err) {
          this.ajaxErrors.push(
            `Nie można załadować listy wydrukowanych dokumentów - upewnij się, że w lokalizacji txt/ istnieje plik ${apiUrl}`,
          );
          console.error(err);
        }
      });
    },
    async fetchDriveData(vin) {
      this.loadStatus = {
        ...this.loadStatus,
        [vin]: "loading",
      };
      console.log(`action fetch /api/drive/${vin}`);
      try {
        const resp = await fetch(`/api/drive/${vin}`);
        const data = await resp.json();
        const value = JSON.stringify(data, null, 2);
        this.driveDataStr = value;
        this.loadStatus[vin] = "loaded";
      } catch (err) {
        console.error(err);
        this.loadStatus[vin] = "failed";
      }
    },
  },
});
