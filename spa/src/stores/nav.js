import menu from "./menu.json" with { type: "json" };
import { defineStore } from "pinia";
// import Vue from 'vue'

export const useNavStore = defineStore("nav", {
  state: () => {
    return {
      menu: menu.menu,
      links: menu.links,
      drawerOpen: false, // Vue.prototype.$q.platform.is.desktop,
      translateTab: "translator",
      conversionsTab: "due-docs",
      selectedBrand: {},
      selectedType: "",
      selectedVariant: "",
      selectedVersion: "",
    };
  },
  getters: {
    selectedXml: (state) =>
      `${state.selectedType.brand}_${state.selectedType.value}`,
  },
  actions: {
    selectBrand(brand) {
      if ("value" in this.selectedBrand && brand !== this.selectedBrand) {
        this.selectedType = "";
        this.selectedVariant = "";
        this.selectedVersion = "";
      }
      this.selectedBrand = brand;
    },
  },
});
