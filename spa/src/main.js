import { createApp } from "vue";
import { createPinia } from "pinia";
import { Meta, Notify, Quasar } from "quasar";
import { createRouter, createWebHashHistory } from "vue-router";
import routes from "./router/routes.ts";
import "@quasar/extras/fontawesome-v6/fontawesome-v6.css";
import "@quasar/extras/material-icons/material-icons.css";
import "quasar/dist/quasar.css";
import "./style.css";
import App from "./App.vue";

const myApp = createApp(App);

myApp.use(createRouter({
  history: createWebHashHistory(),
  routes,
}));

myApp.use(createPinia());

myApp.use(Quasar, {
  plugins: {
    Meta,
    Notify,
  },
});

myApp.mount("#app");
