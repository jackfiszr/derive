export function bodyCodeToBodyName(bodyCode) {
  return {
    AA: "Sedan",
    AB: "Hatchback",
    AC: "Combi",
    AD: "Coupe",
    AE: "Cabrio",
    AF: "(wielozadaniowy)",
    AG: "Combi (ciężarowy)",
    BB: "VAN",
  }[bodyCode];
}

export function isFourWheelDrive(versionData) {
  const version = String(versionData["_0.2.VERSION"]);
  const numberOfSteeredAxles = versionData["_3.ANTRIEBSACHS"];
  return (
    numberOfSteeredAxles === 2 ||
    version.charAt(0) === "A" ||
    version.charAt(1) === "A" ||
    version.charAt(0) === "Q" ||
    version.charAt(1) === "Q"
  );
}

export function engineDesc(versionData) {
  const version = String(versionData["_0.2.VERSION"]);
  const capacity = (Math.round(versionData["_25.HUBRAUM"] / 100) / 10)
    .toFixed(1).replace(".", ",");
  const power = Math.round(versionData["_27.1.NENNLEISTKW"]);
  const horsePower = Math.round(power * 1.36);
  let result = `${capacity} / ${power} kW (${horsePower} KM) `;
  if (version.charAt(1) === "D" || version.charAt(2) === "D") {
    result += " DSG";
  }
  if (isFourWheelDrive(versionData)) {
    result += " 4x4";
  }
  return result;
}

/* function xmlError () {
  $('#xml-alert').html(`<div class="alert alert-danger" role="alert">
                <strong>Nie można załadować danych XML!</strong>
                <br>W przypadku uruchamiania strony przez protokół file:// należy używać przeglądarki Mozilla Firefox:
                <br><a href="https://www.mozilla.org/pl/firefox/new/?scene=2" target="_blank">https://www.mozilla.org/pl/firefox/new/?scene=2</a>
              </div>`)
}

function minToMax (arr) {
  var sorted = arr.sort(sortNumber)
  var minValue = sorted[0]
  var maxValue = sorted[sorted.length-1]
  if (minValue === maxValue) {
    return minValue
  } else {
    return `${minValue} - ${maxValue} **`
  }
}
function sortNumber (a, b) {
  return a - b
}

function brandNameToBrandCode (brandName) {
  return brandName.substring(0, 2).toLowerCase().replace('vo', 'vw')
}

function homologationXmlToJson (xmlData) {
  const approvalInfo = {
    brand:                      $(xmlData).find("_0\\.1\\.FABRIKMARKE:first").text(),
    model:                      $(xmlData).find("_0\\.2\\.1\\.HANDELSNAME:first").text(),
    type:                       $(xmlData).find('_0\\.2\\.TYP').text(),
    homologationNumber:         $(xmlData).find('_GENEHMIGUNGSNR').text(),
    homologationDate:           $(xmlData).find('_GENEHMIGUNGSDAT').text(),
    manufacturerNameAndAddress: $(xmlData).find('_0\\.5\\.ANSCHRIFT').text()
  }
  const vvData = {}
  $(xmlData).find('M1vollst').each(function() {
    const variant = $(this).find("_0\\.2\\.VARIANTE").text()
    if (!vvData[variant]) vvData[variant] = {}
    const version = $(this).find("_0\\.2\\.VERSION").text()
    vvData[variant][version] = {}
    vvData[variant][version].brandName = $(this).find('_0\\.1\\.FABRIKMARKE').text()
    vvData[variant][version].tradeName = $(this).find('_0\\.2\\.1\\.HANDELSNAME').text()
    vvData[variant][version].category = $(this).find('_0\\.4\\.FAHRZEUGKLASSE').text()
    vvData[variant][version].manufacturerPlatePosition = $(this).find('_0\\.6\\.TX_ANBRING_FABSCHILD').text()
    vvData[variant][version].vinPosition = $(this).find('_0\\.6\\.TX_ANBRING_FZIDNR').text()
    vvData[variant][version].axlesNumber = $(this).find('_1\\.ANZACHS').text()
    vvData[variant][version].wheelsNumber = $(this).find('_1\\.ANZRAEDER').text()
    vvData[variant][version].drivingAxlesNumber = $(this).find('_3\\.ANTRIEBSACHS').text()
    vvData[variant][version].drivingAxlesPosition = $(this).find('_3\\.TX_LAGEANTRIEBSACHS').text()
    vvData[variant][version].drivingAxlesConnection = $(this).find('_3\\.VERBINDANTRIEBSACHS').text()
    vvData[variant][version].wheelbase = $(this).find('_4\\.RADSTAND').text()
    vvData[variant][version].axlesDistance = $(this).find('_4\\.1\\.ACHSABSTAND12').text()
    vvData[variant][version].minimumLength = $(this).find('_5\\.LAENGEMIN').text()
    vvData[variant][version].maximumLength = $(this).find('_5\\.LAENGEMAX').text()
    vvData[variant][version].minimumWidth = $(this).find('_6\\.BREITEMIN').text()
    vvData[variant][version].minimumHeight = $(this).find('_7\\.HOEHEMIN').text()
    vvData[variant][version].maximumHeight = $(this).find('_7\\.HOEHEMAX').text()
    vvData[variant][version].runningOrderMass = $(this).find('_13\\.MASSEFAHRBEREITMIN').text()
    vvData[variant][version].permissibleMaximumMass = $(this).find('_16\\.1\\.TECHZULGESAMTM').text()
    vvData[variant][version].permissibleMaximumAxle1Mass = $(this).find('_16\\.2\\.ZULMAXACHSLAST1').text()
    vvData[variant][version].permissibleMaximumAxle2Mass = $(this).find('_16\\.2\\.ZULMAXACHSLAST2').text()
    vvData[variant][version].permissibleMaximumWithTrailerMass = $(this).find('_16\\.4\\.GESAMTM_FZKOMBI').text()
    vvData[variant][version].centreAxleTrailerMass = $(this).find('_18\\.3\\.ZENTRALACHSANH').text()
    vvData[variant][version].unbrakedTrailerMass = $(this).find('_18\\.4\\.ZULANHLASTUNGEBR').text()
    vvData[variant][version].towingDeviceMaximumLoad = $(this).find('_19\\.STUETZLAST').text()
    vvData[variant][version].engineManufacturer = $(this).find('_20\\.HERSTANTRIEBSMASCH').text()
    vvData[variant][version].engineDesignationType = $(this).find('_21\\.BAUMUSTER').text() // engine code
    vvData[variant][version].engineWorkingProcedure = $(this).find('_22\\.TX_ARBEITSVERF').text() // working principle
    vvData[variant][version].electricOnly = $(this).find('_23\\.TX_ELEKTRISCH').text()
    vvData[variant][version].electricHybrid = $(this).find('_23\\.1\\.TX_HYBRIDELEKTRISCH').text()
    vvData[variant][version].cylindersNumberAndGrouping = $(this).find('_24\\.TX_ANZANORDZYL').text()
    vvData[variant][version].engineCapacity = $(this).find('_25\\.HUBRAUM').text()
    vvData[variant][version].fuelCode = $(this).find('_26\\.CODE_KRAFTSTOFF').text()
    vvData[variant][version].fuelType = $(this).find('_26\\.TX_KRAFTSTOFF').text()
    vvData[variant][version].multiFuelType = $(this).find('_26\\.1\\.TX_KRAFTSTOFFART').text()
    vvData[variant][version].engineNetPower = $(this).find('_27\\.NENNLEISTKW').text() || $(this).find('_27\\.1\\.NENNLEISTKW').text()
    vvData[variant][version].engineRotationSpeedNetPower = $(this).find('_27\\.BEIUMDREH').text() || $(this).find('_27\\.1\\.BEIUMDREH').text()
    vvData[variant][version].maximumSpeed = $(this).find('_29\\.HOECHSTGESCHW').text()
    vvData[variant][version].minimumAxle1TrackWidth = $(this).find('_30\\.SPURWEITE1MIN').text()
    vvData[variant][version].maximumAxle1TrackWidth = $(this).find('_30\\.SPURWEITE1MAX').text()
    vvData[variant][version].minimumAxle2TrackWidth = $(this).find('_30\\.SPURWEITE2MIN').text()
    vvData[variant][version].maximumAxle2TrackWidth = $(this).find('_30\\.SPURWEITE2MAX').text()
    vvData[variant][version].axle1Tyres = $(this).find('_35\\.BEREIFACHSE1').text()
    vvData[variant][version].axle1Rim = $(this).find('_35\\.FELGEACHSE1').text()
    vvData[variant][version].axle2Tyres = $(this).find('_35\\.BEREIFACHSE2').text()
    vvData[variant][version].axle2Rim = $(this).find('_35\\.FELGEACHSE2').text()
    vvData[variant][version].trailerBrakeSystemConnection = $(this).find('_36\\.TX_ANHBREMSVERB').text()
    vvData[variant][version].bodyType = $(this).find('_38\\.CODE_AUFBAU').text()
    vvData[variant][version].doorsNumberAndPosition = $(this).find('_41\\.TX_ANZANORDTUEREN').text()
    vvData[variant][version].seatsNumber = $(this).find('_42\\.ANZSITZE').text()
    vvData[variant][version].standingNoise = $(this).find('_46\\.STANDGERAEUSCH').text()
    vvData[variant][version].rotationSpeedNoise = $(this).find('_46\\.DREHZSTANDGERAEUSCH').text()
    vvData[variant][version].drivingNoise = $(this).find('_46\\.FAHRGERAEUSCH').text()
    vvData[variant][version].exhaustEmissionEuroLevel = $(this).find('_47\\.EUROSTUFE').text()
    vvData[variant][version].exhaustEmissionGuidelineNumber = $(this).find('_48\\.ABGASRICHTL').text()
    vvData[variant][version].exhaustEmissionTest1MainCo = $(this).find('_48\\.PRFTYPICO').text()
    vvData[variant][version].exhaustEmissionTest1MainThc = $(this).find('_48\\.PRFTYPITHC').text()
    vvData[variant][version].exhaustEmissionTest1MainNmhc = $(this).find('_48\\.PRFTYPINMHC').text()
    vvData[variant][version].exhaustEmissionTest1MainNox = $(this).find('_48\\.PRFTYPINOX').text()
    vvData[variant][version].exhaustEmissionTest1MainParticulatesMass = $(this).find('_48\\.PRFTYPIPARTIKEL').text()
    vvData[variant][version].exhaustEmissionTest1MainParticulatesNumber = $(this).find('_48\\.PRFTYPIPARTIKELZAHL').text()
    vvData[variant][version].exhaustEmissionTest1MainParticulatesExponent = $(this).find('_48\\.PRFTYPIPARTIKELZEXP').text()
    vvData[variant][version].exhaustEmissionCO2PetrolAndDieselInUrbanCondition = $(this).find('_49\\.CO2INNERORTBENZINDIESEL').text()
    vvData[variant][version].exhaustEmissionCO2PetrolAndDieselInExtraUrbanCondition = $(this).find('_49\\.CO2AUSSORTBENZINDIESEL').text()
    vvData[variant][version].exhaustEmissionCO2PetrolAndDieselInCombinedCondition = $(this).find('_49\\.CO2KOMBIBENZINDIESEL').text()
    vvData[variant][version].fuelConsumptionPetrolAndDieselInUrbanCondition = $(this).find('_49\\.VERBRLITERINNORT').text()
    vvData[variant][version].fuelConsumptionPetrolAndDieselInExtraUrbanCondition = $(this).find('_49\\.VERBRLITERAUSSORT').text()
    vvData[variant][version].fuelConsumptionPetrolAndDieselInCombinedCondition = $(this).find('_49\\.VERBRLITERKOMBI').text()
    vvData[variant][version].ecoInnovationEquipped = $(this).find('_49\\.3\\.TX_OEKOINNOVATION').text()
    vvData[variant][version].remarks = $(this).find('_52\\.ANMERKUNGEN').text()
    // vvData[variant][version].internalManufacturerData = $(this).find('_INTERNE_HERST_DATEN').text()
    // vvData[variant][version].remarksAndExceptions = $(this).find('_22_BEMERKUNGEN_AUSNAHMEN').text()
    // vvData[variant][version].emissionsClass = $(this).find('_14\\.1_SLD').text()
    vvData[variant][version].exhaustEmissionTest1MainThcNox = $(this).find('_48\\.PRFTYPIHCPLUSNOX').text()
    vvData[variant][version].smokingAbsorptionFactor = $(this).find('_48\\.1\\.RAUCH').text()
    vvData[variant][version].exhaustEmissionTest2BivCo = $(this).find('_48\\.PRFTYPIBIVCO').text()
    vvData[variant][version].exhaustEmissionTest1BivThc = $(this).find('_48\\.PRFTYPIBIVTHC').text()
    vvData[variant][version].exhaustEmissionTest1BivNmhc = $(this).find('_48\\.PRFTYPIBIVNMHC').text()
    vvData[variant][version].exhaustEmissionTest1BivNox = $(this).find('_48\\.PRFTYPIBIVNOX').text()
    vvData[variant][version].exhaustEmissionCO2GasInUrbanCondition = $(this).find('_49\\.CO2INNERORTGAS').text()
    vvData[variant][version].exhaustEmissionCO2GasInExtraUrbanCondition = $(this).find('_49\\.CO2AUSSORTGAS').text()
    vvData[variant][version].exhaustEmissionCO2GasInCombinedCondition = $(this).find('_49\\.CO2KOMBIGAS').text()
    vvData[variant][version].fuelConsumptionGasInUrbanCondition = $(this).find('_49\\.VERBRKUBIKMINNORT').text()
    vvData[variant][version].fuelConsumptionGasInExtraUrbanCondition = $(this).find('_49\\.VERBRKUBIKMAUSSORT').text()
    vvData[variant][version].fuelConsumptionGasInCombinedCondition = $(this).find('_49\\.VERBRKUBIKMKOMBI').text()
  })
  return {
    approvalInfo,
    vvData
  }
}
*/
