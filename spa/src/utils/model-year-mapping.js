export default [{
  "M": "1961",
  "N": "1962",
  "P": "1963",
  "R": "1964",
  "S": "1965",
  "T": "1966",
  "V": "1967",
  "W": "1968",
  "X": "1969",
  "Y": "1970",
}, {
  "1": "1971",
  "2": "1972",
  "3": "1973",
  "4": "1974",
  "5": "1975",
  "6": "1976",
  "7": "1977",
  "8": "1978",
  "9": "1979",
  "A": "1980",
  "B": "1981",
  "C": "1982",
  "D": "1983",
  "E": "1984",
  "F": "1985",
  "G": "1986",
  "H": "1987",
  "J": "1988",
  "K": "1989",
  "L": "1990",
  "M": "1991",
  "N": "1992",
  "P": "1993",
  "R": "1994",
  "S": "1995",
  "T": "1996",
  "V": "1997",
  "W": "1998",
  "X": "1999",
  "Y": "2000",
}, {
  "1": "2001",
  "2": "2002",
  "3": "2003",
  "4": "2004",
  "5": "2005",
  "6": "2006",
  "7": "2007",
  "8": "2008",
  "9": "2009",
  "A": "2010",
  "B": "2011",
  "C": "2012",
  "D": "2013",
  "E": "2014",
  "F": "2015",
  "G": "2016",
  "H": "2017",
  "J": "2018",
  "K": "2019",
  "L": "2020",
  "M": "2021",
  "N": "2022",
  "P": "2023",
  "R": "2024",
  "S": "2025",
  "T": "2026",
  "V": "2027",
  "W": "2028",
  "X": "2029",
  "Y": "2030",
}, {
  "1": "2031",
  "2": "2032",
  "3": "2033",
  "4": "2034",
  "5": "2035",
  "6": "2036",
  "7": "2037",
  "8": "2038",
  "9": "2039",
  "A": "2040",
  "B": "2041",
  "C": "2042",
  "D": "2043",
  "E": "2044",
  "F": "2045",
  "G": "2046",
  "H": "2047",
  "J": "2048",
  "K": "2049",
  "L": "2050",
  "M": "2051",
  "N": "2052",
  "P": "2053",
  "R": "2054",
  "S": "2055",
  "T": "2056",
  "V": "2057",
  "W": "2058",
  "X": "2059",
  "Y": "2060",
}];
