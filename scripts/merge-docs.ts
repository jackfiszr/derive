import { degrees, PDFDocument } from "pdf-lib";
import log from "../logger.ts";

const MERGE_DIR = "./docs/merge";

main();

async function main() {
  const docsToMerge = Array.from(Deno.readDirSync(MERGE_DIR)).map((el) =>
    `${MERGE_DIR}/${el.name}`
  ).sort();
  console.log(docsToMerge);
  if (docsToMerge.length > 0) {
    const pdfBytes = await mergeDocs(docsToMerge);
    const filename = "merged_full.pdf";
    const filepath = `../${filename}`;
    Deno.writeFileSync(filepath, pdfBytes);
    log.info(`OK: dokumenty zostały połączone w pliku ${filepath}\n`, [
      "green",
    ]);
    clearMergeDir(docsToMerge);
  }
}

async function mergeDocs(docsToMerge: string[]) {
  const mergedPdf = await PDFDocument.create();
  for (const pdfPath of docsToMerge) {
    const pdfBytes = Deno.readFileSync(pdfPath);
    const pdfDoc = await PDFDocument.load(pdfBytes);
    if (pdfDoc.getPageCount() === 1) {
      const [page] = await mergedPdf.copyPages(
        pdfDoc,
        pdfDoc.getPageIndices(),
      );
      mergedPdf.addPage(page);
      mergedPdf.addPage();
    }
    if (pdfDoc.getPageCount() === 2) {
      const [page1, page2] = await mergedPdf.copyPages(
        pdfDoc,
        pdfDoc.getPageIndices(),
      );
      page1.setRotation(degrees(-90));
      page2.setRotation(degrees(90));
      mergedPdf.addPage(page1);
      mergedPdf.addPage(page2);
    }
  }
  const mergedPdfFile = await mergedPdf.save();
  return mergedPdfFile;
}

function clearMergeDir(docsToMerge: string[]) {
  docsToMerge.forEach(async (doc) => await Deno.remove(doc));
}
