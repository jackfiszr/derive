import { ensureDir, exists } from "@std/fs";
import { join } from "@std/path";

export async function getWwwRoot(): Promise<string> {
  const wwwRoot = join(String(import.meta.dirname), "dist");
  const ex = await exists(wwwRoot);
  if (!ex) {
    await ensureDir(wwwRoot);
  }
  return wwwRoot;
}
