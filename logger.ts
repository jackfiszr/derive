import { ensureFile } from "@std/fs";
import { getLogger } from "utils";
import * as cfg from "./config.ts";

await ensureFile(cfg.LOG_FILE_PATH);
export default await getLogger(cfg.LOG_FILE_PATH);
