import { Hono } from "hono";
import {
  driveFetch,
  getDealers,
  getDict,
  getDisp,
  getDoc,
  getDocs,
  getEndpoints,
  getMods,
  getUser,
  printDocs,
  serveWwwRoot,
  xmlEndpoint,
  xmlToJson,
} from "./handlers.ts";

const apiRouter = new Hono();
const spaRouter = new Hono();

spaRouter.get("/*", serveWwwRoot);

apiRouter
  .get("/", getEndpoints)
  .get("/xml", xmlEndpoint)
  .get("/user", getUser)
  .get("/dict", getDict)
  .get("/dealers", getDealers)
  .get("/dispositions", getDisp)
  .get("/modifications", getMods)
  .get("/:model", getDocs)
  .get("/docs/:model/:filename", getDoc)
  .post("/print", printDocs)
  .get("/drive/:vin", driveFetch)
  .get("/xml/:makeType", xmlToJson);

export { apiRouter, spaRouter };
