import { Hono } from "hono";
import { serveStatic } from "hono/deno";
import { APP_HOST, APP_NAME, APP_PORT, APP_VER, USERNAME } from "../config.ts";
import { apiRouter, spaRouter } from "./routing.ts";
import { startHonoApp } from "utils";

const app = new Hono();

app.route("/api", apiRouter);
app.use(
  "/appr/*",
  serveStatic({
    root: "./approvals/.vitepress/dist",
    rewriteRequestPath: (path) => path.replace(/^\/appr/, ""),
  }),
);
// app.use("/*", serveStatic({ root: "./dist" }));
app.route("/", spaRouter);

startHonoApp(app, {
  name: APP_NAME + " " + APP_VER,
  user: USERNAME,
  host: APP_HOST,
  port: APP_PORT as number,
  // open: false,
});
