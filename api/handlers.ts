import { USERNAME } from "../config.ts";
import { getWwwRoot } from "../utils.ts";
import { Context } from "hono";
import log from "../logger.ts";
import { main as drivefetchService } from "./services/driveFetch.ts";
import {
  addToPrintedList,
  printCoc,
  printStatement,
} from "./services/print-docs.js";
import { serveDir } from "@std/http";
import { join } from "@std/path";
import { parse } from "xml";

export async function serveWwwRoot(c: Context) {
  return serveDir(c.req.raw, {
    fsRoot: await getWwwRoot(),
    quiet: true,
  });
}

export function getUser(c: Context) {
  return c.text(JSON.parse(Deno.readTextFileSync("./db/users.json"))[USERNAME]);
}

export function getMods(c: Context) {
  return c.json(JSON.parse(Deno.readTextFileSync("./db/modifications.json")));
}

export function getDisp(c: Context) {
  return c.json(
    Deno.readTextFileSync("./db/dispositions.txt").trim().split(
      "\n",
    ).map((el) => el.trim().split("\t")),
  );
}

export function getDocs(c: Context) {
  return c.json(
    Deno.readTextFileSync(`./db/list/${c.req.param("model")}.txt`).split(
      "\n",
    ).map((el) => el.trim()),
  );
}

export async function getDoc(c: Context) {
  const pdf = await Deno.readFile(
    `./docs/${c.req.param("model")}/${c.req.param("filename")}`,
  );
  c.header("Content-Type", "application/pdf");
  return c.body(pdf);
}

export async function printDocs(c: Context) {
  const body = await c.req.json();
  const data = typeof body === "string" ? JSON.parse(body) : body;
  await printCoc(data);
  await printStatement(data);
  addToPrintedList(data);
  c.status(200);
}

export function getDict(c: Context) {
  return c.json(JSON.parse(Deno.readTextFileSync("./db/dict.json")));
}

export function getDealers(c: Context) {
  return c.json(JSON.parse(Deno.readTextFileSync("./db/dealers.json")));
}

export function getEndpoints(c: Context) {
  const endpoints = [
    "user",
    "modifications",
    "dispositions",
    "fabia",
    "scala",
    "ibiza",
    "rapid2016",
    "rapid2018",
    "rapid2019",
    "dealers",
    "dict",
    "drive/TMBZZZM0CKV1N0000",
    "xml",
  ];
  let html = "<!DOCTYPE html><html><body>";
  endpoints.forEach((endpoint) => {
    html += `<a href="/api/${endpoint}">/${endpoint}</a><br>`;
  });
  html += "</body></html>";
  return c.html(html);
}

export async function driveFetch(c: Context) {
  const vin = `${c.req.param("vin")}`;
  const dataDir = vin.includes("ZZZM0CKV1N") ? "mock" : "data";
  const dataFile = `./db/${dataDir}/${vin}.json`;
  let data = null;
  try {
    data = JSON.parse(Deno.readTextFileSync(dataFile));
    log.info(`\n${vin}: Dane już wcześniej pobrane`, ["bold", "cyan"]);
    log.info(` > ${dataFile}`, ["gray"]);
  } catch (_err) {
    log.info(`\n${vin}: Pobieranie danych z Drive`, [
      "bold",
      "white",
      "bgBlue",
    ]);
    try {
      await drivefetchService(vin);
      let endColor: "bgGreen" | "bgRed";
      try {
        data = JSON.parse(Deno.readTextFileSync(dataFile));
        endColor = "bgGreen";
      } catch (_err) {
        // data not saved
        endColor = "bgRed";
      }
      log.info(`\n${vin}: Zakończono pobieranie danych`, [
        "bold",
        "white",
        endColor,
      ]);
    } catch (error) {
      log.error(`\nBŁĄD: ${error}`);
      // return c.text(error);
    }
  }
  if (data) {
    return c.json(data);
  } else {
    return c.notFound();
  }
}

export async function xmlToJson(c: Context) {
  try {
    const xmlFilePath = join("db", "xml", `${c.req.param("makeType")}.xml`);
    const xmlFile = await Deno.open(xmlFilePath);
    // const { size } = await xmlFile.stat();
    return c.json(parse(xmlFile, {
      // progress(bytes) {
      //   Deno.stdout.writeSync(
      //     new TextEncoder().encode(
      //       `Parsowanie ${xmlFilePath}: ${(100 * bytes / size).toFixed(2)}%\r`,
      //     ),
      //   );
      // },
    }));
  } catch (error) {
    c.status(404);
    return c.body(String(error));
  }
}

export function xmlEndpoint(c: Context) {
  const listOfXmlFiles = Array.from(Deno.readDirSync(join("db", "xml"))).map(
    (el) => el.name.replace(".xml", ""),
  ).sort();
  let html = "<!DOCTYPE html><html><body>";
  listOfXmlFiles.forEach((xml) => {
    html += `<a href="xml/${xml}">/${xml}</a><br>`;
  });
  html += "</body></html>";
  return c.html(html);
}
