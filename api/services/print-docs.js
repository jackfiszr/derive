// deno-fmt-ignore-file
import { PDFDocument, PageSizes, grayscale } from 'pdf-lib'
import fontkit from '@pdf-lib/fontkit'
import log from '../../logger.ts'

const fonts = {
  Skoda: {
    normal: 'fonts/SKODANext-Regular.ttf',
    bold: 'fonts/SKODANext-Bold.ttf',
    italics: 'fonts/SKODANext-Italic.ttf',
    bolditalics: 'fonts/SKODANext-MediumItalic.ttf'
  }
}

const manufacturers = {
  skoda: ['SKODA AUTO a.s.', 'Tr. Vaclava Klementa 869', '293 60 Mlada Boleslav CZ'],
  seat: ['SEAT, S.A.', 'Autov-a A-2, Km. 585', 'ES-08760 Martorell']
}

export async function printCoc (data) {
  const pdfDoc = await PDFDocument.create()
  pdfDoc.registerFontkit(fontkit)
  const normal = await pdfDoc.embedFont(Deno.readFileSync(fonts.Skoda.normal))
  const bold = await pdfDoc.embedFont(Deno.readFileSync(fonts.Skoda.bold))
  const fontSize = 7
  const pages = []
  const pageSize = [...PageSizes.A4].reverse()
  const numOfPages = 2;
  [...Array(numOfPages).keys()].forEach(_ => pages.push(pdfDoc.addPage(pageSize)))
  pages.forEach(page => {
    page.setFont(normal)
    page.setFontSize(fontSize)
    page.setLineHeight(10)
  })
  const [ pageWidth, pageHeight ] = pageSize
  const topMargin = pageHeight - 25
  const leftMargin = 20
  try {
    const uint8Array = Deno.readFileSync('images/vgp.png')
    const logoImage = await pdfDoc.embedPng(uint8Array)
    const logoDims = logoImage.scale(0.1)
    pages[0].drawImage(logoImage, {
      x: pageWidth - logoDims.width - 15,
      y: pageHeight - logoDims.height - 15,
      width: logoDims.width,
      height: logoDims.height
    })
  } catch (e) {
    console.error(e)
  }

  function drawAt ({ page, row, col, text, font, size, color } ) {
    const rowHeight = page === 1 ? pageHeight / 64 : pageHeight / 57
    const colWidth = pageWidth / 24
    if (page >= numOfPages * 2) page = 0
    const pagesIndex = Math.floor(page / 2)
    let x = leftMargin, y = topMargin
    if (page % 2 === 1) {
      x += pageWidth / 2
    }
    x += (col - 1) * colWidth
    y -= (row - 1) * rowHeight
    pages[pagesIndex].drawText(text, { x, y, font, size, color })
  }

  drawAt({ page: 1, row: 1, col: 1, text: 'VOLKSWAGEN GROUP POLSKA', font: bold, size: 10 })
  drawAt({ page: 1, row: 2, col: 1, text: 'POJAZDY SKOMPLETOWANE' })
  drawAt({ page: 1, row: 4, col: 1, text: 'ŚWIADECTWO ZGODNOŚCI WE', font: bold, size: 9 })
  drawAt({ page: 1, row: 6, col: 1, text: `Niżej podpisany ${data.user} niniejszym zaświadcza, że pojazd:` })
  drawAt({ page: 1, row: 7, col: 1, text: '0.1.' })
  drawAt({ page: 1, row: 7, col: 2, text: 'Marka (nazwa handlowa producenta):' })
  drawAt({ page: 1, row: 7, col: 7, text: data.brandName, font: bold })
  drawAt({ page: 1, row: 8, col: 1, text: '0.2.' })
  drawAt({ page: 1, row: 8, col: 2, text: 'Typ:' })
  drawAt({ page: 1, row: 8, col: 7, text: data.vehicleType2ndStage, font: bold })
  drawAt({ page: 1, row: 9, col: 2, text: 'Wariant:' })
  drawAt({ page: 1, row: 9, col: 7, text: data.variant2ndStage, font: bold })
  drawAt({ page: 1, row: 10, col: 2, text: 'Wersja:' })
  drawAt({ page: 1, row: 10, col: 7, text: data.version2ndStage, font: bold })
  drawAt({ page: 1, row: 11, col: 1, text: '0.2.1.' })
  drawAt({ page: 1, row: 11, col: 2, text: 'Nazwa handlowa:' })
  drawAt({ page: 1, row: 11, col: 7, text: data.tradeName2ndStage, font: bold })
  drawAt({ page: 1, row: 12, col: 1, text: '0.2.2.' })
  drawAt({ page: 1, row: 12, col: 2, text: 'W przypadku pojazdów homologowanych wielostopniowo,' })
  drawAt({ page: 1, row: 13, col: 2, text: 'informacje dotyczące homologacji typu pojazdu podstawowego:' })
  drawAt({ page: 1, row: 14, col: 2, text: 'Typ:' })
  drawAt({ page: 1, row: 14, col: 7, text: data.vehicleType })
  drawAt({ page: 1, row: 15, col: 2, text: 'Wariant:' })
  drawAt({ page: 1, row: 15, col: 7, text: data.variant })
  drawAt({ page: 1, row: 16, col: 2, text: 'Wersja:' })
  drawAt({ page: 1, row: 16, col: 7, text: data.version })
  drawAt({ page: 1, row: 17, col: 2, text: 'Numer homologacji typu, numer rozszerzenia:' })
  drawAt({ page: 1, row: 17, col: 7, text: data.homologationNumber })
  drawAt({ page: 1, row: 18, col: 1, text: '0.2.2.1.' })
  drawAt({ page: 1, row: 18, col: 2, text: `Dopuszczalne wartości parametrów w ramach wielostopniowej homologacji typu
przeprowadzanej przy wykorzystaniu wartości emisji zanieczyszczeń
generowanych przez pojazd podstawowy (w stosownych przypadkach należy podać zakres):` })
  drawAt({ page: 1, row: 21, col: 2, text: 'Rzeczywista masa pojazdu końcowego [kg]:' })
  drawAt({ page: 1, row: 21, col: 9, text: `${data.actualWeight2ndStage}`, font: bold })
  drawAt({ page: 1, row: 22, col: 2, text: 'Maksymalna masa całkowita pojazdu końcowego [kg]:' })
  drawAt({ page: 1, row: 22, col: 9, text: `${data.permissibleMaximumWeight}`, font: bold })
  drawAt({ page: 1, row: 23, col: 2, text: 'Powierzchnia czołowa pojazdu końcowego [cm²]:' })
  drawAt({ page: 1, row: 23, col: 9, text: '----------' })
  drawAt({ page: 1, row: 24, col: 2, text: 'Opór toczenia [kg/t]:' })
  drawAt({ page: 1, row: 24, col: 9, text: '----------' })
  drawAt({ page: 1, row: 25, col: 2, text: 'Pole przekroju poprzecznego przepływu powietrza przez maskownicę [cm²]:' })
  drawAt({ page: 1, row: 25, col: 9, text: '----------' })
  drawAt({ page: 1, row: 26, col: 1, text: '0.2.3.' })
  drawAt({ page: 1, row: 26, col: 2, text: 'Identyfikatory (w stosownych przypadkach)' })
  drawAt({ page: 1, row: 27, col: 1, text: '0.2.3.1.' })
  drawAt({ page: 1, row: 27, col: 2, text: 'Identyfikator rodziny interpolacji:' })
  drawAt({ page: 1, row: 27, col: 7, text: data.familyInterpolationIdentifier || '----------' })
  drawAt({ page: 1, row: 28, col: 1, text: '0.2.3.2.' })
  drawAt({ page: 1, row: 28, col: 2, text: 'Identyfikator rodziny ATCT:' })
  drawAt({ page: 1, row: 28, col: 7, text: data.familyAtctIdentifier || '----------' })
  drawAt({ page: 1, row: 29, col: 1, text: '0.2.3.3.' })
  drawAt({ page: 1, row: 29, col: 2, text: 'Identyfikator rodziny PEMS:' })
  drawAt({ page: 1, row: 29, col: 7, text: data.familyPemsIdentifier || '----------' })
  drawAt({ page: 1, row: 30, col: 1, text: '0.2.3.4.' })
  drawAt({ page: 1, row: 30, col: 2, text: 'Identyfikator rodziny obciążenia drogowego:' })
  drawAt({ page: 1, row: 30, col: 7, text: data.familyRoadloadIdentifier || '----------' })
  drawAt({ page: 1, row: 31, col: 1, text: '0.2.3.5.' })
  drawAt({ page: 1, row: 31, col: 2, text: 'Identyfikator rodziny macierzy obciążenia drogowego\n(w stosownych przypadkach):' })
  drawAt({ page: 1, row: 32, col: 7, text: data.familyRoadloadMatrixIdentifier || '----------' })
  drawAt({ page: 1, row: 33, col: 1, text: '0.2.3.6.' })
  drawAt({ page: 1, row: 33, col: 2, text: 'Identyfikator rodziny okresowej regeneracji:' })
  drawAt({ page: 1, row: 33, col: 7, text: data.familyPeriodicRegenerationIdentifier || '----------' })
  drawAt({ page: 1, row: 34, col: 1, text: '0.2.3.7.' })
  drawAt({ page: 1, row: 34, col: 2, text: 'Identyfikator rodziny badania emisji par:' })
  drawAt({ page: 1, row: 34, col: 7, text: data.familyEvaporativeTestIdentifier || '----------' })
  drawAt({ page: 1, row: 35, col: 1, text: '0.4.' })
  drawAt({ page: 1, row: 35, col: 2, text: 'Kategoria pojazdu:' })
  drawAt({ page: 1, row: 35, col: 7, text: 'N1', font: bold })
  drawAt({ page: 1, row: 36, col: 1, text: '0.5.' })
  drawAt({ page: 1, row: 36, col: 2, text: 'Nazwa przedsiębiorstwa' })
  drawAt({ page: 1, row: 37, col: 2, text: 'i adres producenta:' })
  drawAt({ page: 1, row: 36, col: 7, text: 'Volkswagen Group Polska Sp. z o.o.' })
  drawAt({ page: 1, row: 37, col: 7, text: 'ul. Krańcowa 44, 61-037 Poznań' })
  drawAt({ page: 1, row: 38, col: 1, text: '0.5.1.' })
  drawAt({ page: 1, row: 38, col: 2, text: 'W przypadku pojazdów homologowanych' })
  drawAt({ page: 1, row: 39, col: 2, text: 'wielostopniowo, nazwa przedsiębiorstwa i adres' })
  drawAt({ page: 1, row: 40, col: 2, text: 'producenta pojazdu podstawowego:' })
  drawAt({ page: 1, row: 38, col: 7, text: manufacturers[data.brandName.toLowerCase()][0] })
  drawAt({ page: 1, row: 39, col: 7, text: manufacturers[data.brandName.toLowerCase()][1] })
  drawAt({ page: 1, row: 40, col: 7, text: manufacturers[data.brandName.toLowerCase()][2] })
  drawAt({ page: 1, row: 41, col: 1, text: '0.6.' })
  drawAt({ page: 1, row: 41, col: 2, text: 'Umiejscowienie' })
  drawAt({ page: 1, row: 42, col: 2, text: 'i sposób umieszczenia tabliczek znamionowych:' })
  drawAt({ page: 1, row: 42, col: 7, text: 'przyklejona na słupku \'B\'' })
  drawAt({ page: 1, row: 43, col: 2, text: 'Umiejscowienie numeru identyfikacyjnego pojazdu:' })
  drawAt({ page: 1, row: 43, col: 7, text: 'w komorze silnika po prawej' })
  drawAt({ page: 1, row: 44, col: 1, text: '0.10.' })
  drawAt({ page: 1, row: 44, col: 2, text: 'Numer identyfikacyjny pojazdu:' })
  drawAt({ page: 1, row: 44, col: 7, text: data.vin, font: bold })
  drawAt({ page: 1, row: 45, col: 1, text: '0.11.' })
  drawAt({ page: 1, row: 45, col: 2, text: 'Data produkcji pojazdu:' })
  drawAt({ page: 1, row: 45, col: 7, text: data.productionDate, font: bold })
  drawAt({ page: 1, row: 46, col: 2, text: 'a) został skompletowany' })
  drawAt({ page: 1, row: 47, col: 2, text: 'i zmieniony w następujący sposób:' })
  drawAt({ page: 1, row: 46, col: 7, text: 'przystosowanie do przewozu ładunków,', font: bold })
  drawAt({ page: 1, row: 47, col: 7, text: `zmiana liczby miejsc z ${data.seatsAmount} na 2`, font: bold })
  drawAt({ page: 1, row: 48, col: 1, text: 'oraz' })
  drawAt({ page: 1, row: 48, col: 2, text: 'b) odpowiada pod każdym względem typowi' })
  drawAt({ page: 1, row: 49, col: 2, text: 'opisanemu w homologacji:' })
  drawAt({ page: 1, row: 49, col: 7, text: data.homologationNumber2ndStage, font: bold })
  drawAt({ page: 1, row: 50, col: 2, text: 'wydanej dnia:' })
  drawAt({ page: 1, row: 50, col: 7, text: data.homologationDate2ndStage, font: bold })
  drawAt({ page: 1, row: 51, col: 2, text: 'c) może być zarejestrowany na stałe w państwach członkowskich o ruchu prawostronnym' })
  drawAt({ page: 1, row: 52, col: 2, text: 'stosujących jednostki metryczne w prędkościomierzach oraz jednostki metryczne w hodometrach' })
  drawAt({ page: 1, row: 54, col: 1, text: `Poznań, ${data.date}`, size: 8 })
  drawAt({ page: 1, row: 60, col: 5, text: `Nr komisji: ${data.commissionNumber} | Nr dealera: ${data.dealerNumber}`, color: grayscale(0.5) })
  drawAt({ page: 2, row: 1, col: 1, text: '1.' })
  drawAt({ page: 2, row: 1, col: 2, text: 'Liczba osi i kół:' })
  drawAt({ page: 2, row: 2, col: 1, text: '1.1.' })
  drawAt({ page: 2, row: 2, col: 2, text: 'Liczba i położenie osi z kołami bliźniaczymi:' })
  drawAt({ page: 2, row: 3, col: 1, text: '3.' })
  drawAt({ page: 2, row: 3, col: 2, text: 'Osie napędowe (liczba, pozycja, współpraca):' })
  drawAt({ page: 2, row: 4, col: 1, text: '3.1.' })
  drawAt({ page: 2, row: 4, col: 2, text: 'Stopień automatyzacji pojazdu:' })
  drawAt({ page: 2, row: 5, col: 1, text: '4.' })
  drawAt({ page: 2, row: 5, col: 2, text: 'Rozstaw osi [mm]:' })
  drawAt({ page: 2, row: 6, col: 1, text: '4.1.' })
  drawAt({ page: 2, row: 6, col: 2, text: 'Odstęp między osiami: 1-2 [mm]:' })
  drawAt({ page: 2, row: 7, col: 1, text: '5.' })
  drawAt({ page: 2, row: 7, col: 2, text: 'Długość [mm]:' })
  drawAt({ page: 2, row: 8, col: 1, text: '6.' })
  drawAt({ page: 2, row: 8, col: 2, text: 'Szerokość [mm]:' })
  drawAt({ page: 2, row: 9, col: 1, text: '7.' })
  drawAt({ page: 2, row: 9, col: 2, text: 'Wysokość [mm]:' })
  drawAt({ page: 2, row: 10, col: 1, text: '8.' })
  drawAt({ page: 2, row: 10, col: 2, text: 'Wysunięcie siodła pojazdu ciągnącego naczepę (maksymalne i minimalne) [mm]:' })
  drawAt({ page: 2, row: 11, col: 1, text: '9.' })
  drawAt({ page: 2, row: 11, col: 2, text: 'Odległość między przednim obrysem pojazdu a środkiem urządzenia sprzęgającego [mm]:' })
  drawAt({ page: 2, row: 12, col: 1, text: '11.' })
  drawAt({ page: 2, row: 12, col: 2, text: 'Długość przestrzeni ładunkowej [mm]:' })
  drawAt({ page: 2, row: 12, col: 11, text: data.loadingSurfaceLength2ndStage, font: bold })
  drawAt({ page: 2, row: 13, col: 1, text: '13.' })
  drawAt({ page: 2, row: 13, col: 2, text: 'Masa pojazdu gotowego do jazdy [kg]:' })
  drawAt({ page: 2, row: 13, col: 11, text: `${data.runningOrderVehicleMinimumMass2ndStage}`, font: bold })
  drawAt({ page: 2, row: 14, col: 1, text: '13.1.' })
  drawAt({ page: 2, row: 14, col: 2, text: 'Rozkład tej masy na osie (1./2.) [kg]:' })
  drawAt({ page: 2, row: 15, col: 1, text: '13.2.' })
  drawAt({ page: 2, row: 15, col: 2, text: 'Rzeczywista masa pojazdu [kg]:' })
  drawAt({ page: 2, row: 15, col: 11, text: `${data.actualWeight2ndStage}`, font: bold })
  drawAt({ page: 2, row: 16, col: 1, text: '14.' })
  drawAt({ page: 2, row: 16, col: 2, text: 'Masa pojazdu podstawowego gotowego do jazdy [kg]:' })
  drawAt({ page: 2, row: 16, col: 11, text: `${data.runningOrderVehicleMinimumMass}`, font: bold })
  drawAt({ page: 2, row: 17, col: 1, text: '16.' })
  drawAt({ page: 2, row: 17, col: 2, text: 'Maksymalne masy całkowite' })
  drawAt({ page: 2, row: 18, col: 1, text: '16.1.' })
  drawAt({ page: 2, row: 18, col: 2, text: 'Maksymalna masa całkowita [kg]:' })
  drawAt({ page: 2, row: 19, col: 1, text: '16.2.' })
  drawAt({ page: 2, row: 19, col: 2, text: 'Maksymalna masa przypadająca na każdą oś [kg] 1./2.:' })
  drawAt({ page: 2, row: 20, col: 1, text: '16.4.' })
  drawAt({ page: 2, row: 20, col: 2, text: 'Maksymalna masa całkowita zespołu pojazdów [kg]:' })
  drawAt({ page: 2, row: 21, col: 1, text: '18.' })
  drawAt({ page: 2, row: 21, col: 2, text: 'Technicznie dopuszczalna maksymalna masa ciągnięta przez pojazd w przypadku:' })
  drawAt({ page: 2, row: 22, col: 1, text: '18.1.' })
  drawAt({ page: 2, row: 22, col: 2, text: 'przyczepy z wózkiem skrętnym [kg]:' })
  drawAt({ page: 2, row: 23, col: 1, text: '18.2.' })
  drawAt({ page: 2, row: 23, col: 2, text: 'naczepy [kg]:' })
  drawAt({ page: 2, row: 24, col: 1, text: '18.3.' })
  drawAt({ page: 2, row: 24, col: 2, text: 'przyczepy z osią centralną [kg]:' })
  drawAt({ page: 2, row: 25, col: 1, text: '18.4.' })
  drawAt({ page: 2, row: 25, col: 2, text: 'przyczepy bez hamulca [kg]:' })
  drawAt({ page: 2, row: 26, col: 1, text: '19.' })
  drawAt({ page: 2, row: 26, col: 2, text: 'Technicznie dopuszczalne maksymalne obciążenie statyczne w punkcie sprzęgu [kg]:' })
  drawAt({ page: 2, row: 27, col: 1, text: '20.' })
  drawAt({ page: 2, row: 27, col: 2, text: 'Producent silnika:' })
  drawAt({ page: 2, row: 28, col: 1, text: '21.' })
  drawAt({ page: 2, row: 28, col: 2, text: 'Kod fabryczny silnika oznaczony na silniku:' })
  drawAt({ page: 2, row: 29, col: 1, text: '22.' })
  drawAt({ page: 2, row: 29, col: 2, text: 'Zasada działania:' })
  drawAt({ page: 2, row: 30, col: 1, text: '23.' })
  drawAt({ page: 2, row: 30, col: 2, text: 'Wyłącznie elektryczny:' })
  drawAt({ page: 2, row: 31, col: 1, text: '23.1.' })
  drawAt({ page: 2, row: 31, col: 2, text: 'Klasa pojazdu hybrydowego [elektrycznego]:' })
  drawAt({ page: 2, row: 32, col: 1, text: '24.' })
  drawAt({ page: 2, row: 32, col: 2, text: 'Liczba i położenie cylindrów:' })
  drawAt({ page: 2, row: 33, col: 1, text: '25.' })
  drawAt({ page: 2, row: 33, col: 2, text: 'Pojemność skokowa silnika [cm³]:' })
  drawAt({ page: 2, row: 34, col: 1, text: '26.' })
  drawAt({ page: 2, row: 34, col: 2, text: 'Paliwo:' })
  drawAt({ page: 2, row: 35, col: 1, text: '26.1.' })
  drawAt({ page: 2, row: 35, col: 2, text: 'Jednopaliwowy/dwupaliwowy (bi fuel/dual-fuel)/flex fuel:' })
  drawAt({ page: 2, row: 36, col: 1, text: '26.2.' })
  drawAt({ page: 2, row: 36, col: 2, text: '(Tylko dwupaliwowy (dual-fuel)) typ 1 A/typ 1B/typ 2 A/typ 2B/typ 3B:' })
  drawAt({ page: 2, row: 37, col: 1, text: '27.' })
  drawAt({ page: 2, row: 37, col: 2, text: 'Maksymalna moc:' })
  drawAt({ page: 2, row: 38, col: 1, text: '27.1.' })
  drawAt({ page: 2, row: 38, col: 2, text: 'Maksymalna moc netto [kW] przy [min⁻¹] (silnik spalania wewnętrznego):' })
  drawAt({ page: 2, row: 39, col: 1, text: '27.3.' })
  drawAt({ page: 2, row: 39, col: 2, text: 'Maksymalna moc netto [kW] (silnik elektryczny):' })
  drawAt({ page: 2, row: 40, col: 1, text: '27.4.' })
  drawAt({ page: 2, row: 40, col: 2, text: 'Maksymalna moc 30-minutowa [kW] (silnik elektryczny):' })
  drawAt({ page: 2, row: 41, col: 1, text: '28.' })
  drawAt({ page: 2, row: 41, col: 2, text: 'Skrzynia biegów (rodzaj):' })
  drawAt({ page: 2, row: 42, col: 5, text: 'bieg:' })
  drawAt({ page: 2, row: 42, col: 6.0, text: '1' })
  drawAt({ page: 2, row: 42, col: 6.5, text: '2' })
  drawAt({ page: 2, row: 42, col: 7.0, text: '3' })
  drawAt({ page: 2, row: 42, col: 7.5, text: '4' })
  drawAt({ page: 2, row: 42, col: 8.0, text: '5' })
  drawAt({ page: 2, row: 42, col: 8.5, text: '6' })
  drawAt({ page: 2, row: 42, col: 9.0, text: '7' })
  drawAt({ page: 2, row: 42, col: 9.5, text: '8' })
  drawAt({ page: 2, row: 42, col: 10, text: 'wsteczny' })
  drawAt({ page: 2, row: 43, col: 1, text: '28.1.' })
  drawAt({ page: 2, row: 43, col: 2, text: 'Przełożenia skrzyni biegów:' })
  drawAt({ page: 2, row: 44, col: 1, text: '28.1.1.' })
  drawAt({ page: 2, row: 44, col: 2, text: 'Przełożenie przekładni głównej' })
  drawAt({ page: 2, row: 45, col: 2, text: '(w stosownych przypadkach):' })
  drawAt({ page: 2, row: 46, col: 1, text: '28.1.2.' })
  drawAt({ page: 2, row: 46, col: 2, text: 'Przełożenia przekładni głównej' })
  drawAt({ page: 2, row: 47, col: 2, text: '(w stosownych przypadkach):' })
  drawAt({ page: 2, row: 48, col: 1, text: '29.' })
  drawAt({ page: 2, row: 48, col: 2, text: 'Prędkość maksymalna [km/h]:' })
  drawAt({ page: 2, row: 49, col: 1, text: '30.' })
  drawAt({ page: 2, row: 49, col: 2, text: 'Rozstaw kół osi 1. / 2. [mm]:' })
  drawAt({ page: 2, row: 50, col: 1, text: '35.' })
  drawAt({ page: 2, row: 50, col: 2, text: 'Zamontowany zespół opona/koło / klasa efektywności energetycznej współczynników oporu toczenia (RRC)' })
  drawAt({ page: 2, row: 51, col: 2, text: 'i kategoria opon zastosowana do określenia CO₂ (w stosownych przypadkach):' })
  drawAt({ page: 2, row: 52, col: 1, text: '36.' })
  drawAt({ page: 2, row: 52, col: 2, text: 'Połączenia z hamulcami przyczepy:' })
  drawAt({ page: 2, row: 53, col: 1, text: '37.' })
  drawAt({ page: 2, row: 53, col: 2, text: 'Ciśnienie w przewodzie zasilającym układ hamulcowy przyczepy [kPa]:' })
  drawAt({ page: 3, row: 1, col: 1, text: '38.' })
  drawAt({ page: 3, row: 1, col: 2, text: 'Kod nadwozia:' })
  drawAt({ page: 3, row: 1, col: 10, text: 'BB VAN', font: bold })
  drawAt({ page: 3, row: 2, col: 1, text: '40.' })
  drawAt({ page: 3, row: 2, col: 2, text: 'Kolor pojazdu:' })
  drawAt({ page: 3, row: 3, col: 1, text: '41.' })
  drawAt({ page: 3, row: 3, col: 2, text: 'Liczba i rozmieszczenie drzwi:' })
  drawAt({ page: 3, row: 4, col: 1, text: '42.' })
  drawAt({ page: 3, row: 4, col: 2, text: 'Liczba miejsc siedzących (w tym miejsce kierowcy):' })
  drawAt({ page: 3, row: 4, col: 10, text: '2 (2 z przodu)', font: bold })
  drawAt({ page: 3, row: 5, col: 1, text: '44.' })
  drawAt({ page: 3, row: 5, col: 2, text: 'Numer świadectwa homologacji lub znak homologacji urządzenia sprzęgającego' })
  drawAt({ page: 3, row: 6, col: 2, text: '(jeżeli jest zamontowane):' })
  drawAt({ page: 3, row: 7, col: 1, text: '45.1.' })
  drawAt({ page: 3, row: 7, col: 2, text: 'Wartości charakterystyczne (D/V/S/U):' })
  drawAt({ page: 3, row: 8, col: 1, text: '46.' })
  drawAt({ page: 3, row: 8, col: 2, text: 'Poziom hałasu' })
  drawAt({ page: 3, row: 9, col: 2, text: 'Podczas postoju [dB(A)]:' })
  drawAt({ page: 3, row: 10, col: 2, text: 'przy prędkości obrotowej silnika [min⁻¹]:' })
  drawAt({ page: 3, row: 11, col: 2, text: 'Podczas jazdy [dB(A)]:' })
  drawAt({ page: 3, row: 12, col: 1, text: '47.' })
  drawAt({ page: 3, row: 12, col: 2, text: 'Poziom emisji spalin [EURO]:' })
  drawAt({ page: 3, row: 13, col: 1, text: '47.1.' })
  drawAt({ page: 3, row: 13, col: 2, text: 'Parametry do celów badania emisji Vind' })
  drawAt({ page: 3, row: 14, col: 1, text: '47.1.1.' })
  drawAt({ page: 3, row: 14, col: 2, text: 'Masa próbna [kg]:' })
  drawAt({ page: 3, row: 15, col: 1, text: '47.1.2.' })
  drawAt({ page: 3, row: 15, col: 2, text: 'Powierzchnia czołowa [m²]' })
  drawAt({ page: 3, row: 16, col: 1, text: '47.1.2.1.' })
  drawAt({ page: 3, row: 16, col: 2, text: 'Przewidywana powierzchnia czołowa przepływu powietrza przez maskownicę (w stosownych przypadkach) [cm²]' })
  drawAt({ page: 3, row: 17, col: 1, text: '47.1.3.0.' })
  drawAt({ page: 3, row: 17, col: 2, text: 'Współczynniki obciążenia drogowego' })
  drawAt({ page: 3, row: 18, col: 1, text: '47.1.3.0.' })
  drawAt({ page: 3, row: 18, col: 2, text: 'f₀ [N]:' })
  drawAt({ page: 3, row: 19, col: 1, text: '47.1.3.1.' })
  drawAt({ page: 3, row: 19, col: 2, text: 'f₁ [N/(km/h)]:' })
  drawAt({ page: 3, row: 20, col: 1, text: '47.1.3.2.' })
  drawAt({ page: 3, row: 20, col: 2, text: 'f₂ [N/(km/h)]' })
  drawAt({ page: 3, row: 21, col: 1, text: '47.2.' })
  drawAt({ page: 3, row: 21, col: 2, text: 'Cykl jazdy:' })
  drawAt({ page: 3, row: 22, col: 1, text: '47.2.1.' })
  drawAt({ page: 3, row: 22, col: 2, text: 'Klasa cyklu jazdy:' })
  drawAt({ page: 3, row: 23, col: 1, text: '47.2.2.' })
  drawAt({ page: 3, row: 23, col: 2, text: 'Współczynnik zmniejszenia (fdsc):' })
  drawAt({ page: 3, row: 24, col: 1, text: '47.2.3' })
  drawAt({ page: 3, row: 24, col: 2, text: 'Prędkość graniczna:' })
  drawAt({ page: 3, row: 25, col: 1, text: '48.' })
  drawAt({ page: 3, row: 25, col: 2, text: 'Emisje spalin:' })
  drawAt({ page: 3, row: 26, col: 2, text: 'Numer bazowego aktu prawnego i ostatniego mającego zastosowanie zmieniającego aktu prawnego:' })
  drawAt({ page: 3, row: 27, col: 2, text: '1.2. Procedura badania: Typ 1 (wartości najwyższe WLTP) [mg/km] lub WHSC (EURO VI) [mg/kWh]' })
  drawAt({ page: 3, row: 28, col: 2, text: '/ WHSC (EURO VI)[mg/kWh]' })
  drawAt({ page: 3, row: 29, col: 6, text: 'benzyna/ON' })
  drawAt({ page: 3, row: 29, col: 8, text: 'gaz' })
  drawAt({ page: 3, row: 29, col: 10, text: 'inne' })
  drawAt({ page: 3, row: 30, col: 2, text: 'CO [mg/km]:' })
  drawAt({ page: 3, row: 31, col: 2, text: 'THC [mg/km]:' })
  drawAt({ page: 3, row: 32, col: 2, text: 'NMHC [mg/km]:' })
  drawAt({ page: 3, row: 33, col: 2, text: 'NOx [mg/km]:' })
  drawAt({ page: 3, row: 34, col: 2, text: 'THC+NOx [mg/km]:' })
  drawAt({ page: 3, row: 35, col: 2, text: 'NH₃ [ppm]:' })
  drawAt({ page: 3, row: 36, col: 2, text: 'Cząstki stałe (masa) [mg/km]:' })
  drawAt({ page: 3, row: 37, col: 2, text: 'Cząstki stałe (liczba):' })
  drawAt({ page: 3, row: 39, col: 2, text: '2.2 Procedura badania:  WHTC (EURO VI) [mg/kWh]' })
  drawAt({ page: 3, row: 40, col: 6, text: 'benzyna/ON' })
  drawAt({ page: 3, row: 40, col: 8, text: 'gaz' })
  drawAt({ page: 3, row: 40, col: 10, text: 'inne' })
  drawAt({ page: 3, row: 41, col: 2, text: 'CO [mg/kWh]' })
  drawAt({ page: 3, row: 42, col: 2, text: 'NOx [mg/kWh]:' })
  drawAt({ page: 3, row: 43, col: 2, text: 'NMHC [mg/kWh]:' })
  drawAt({ page: 3, row: 44, col: 2, text: 'THC [mg/kWh]:' })
  drawAt({ page: 3, row: 45, col: 2, text: 'CH₄ [mg/kWh]:' })
  drawAt({ page: 3, row: 46, col: 2, text: 'NH₃ [ppm]:' })
  drawAt({ page: 3, row: 47, col: 2, text: 'Cząstki stałe (masa) [mg/kWh]:' })
  drawAt({ page: 3, row: 48, col: 2, text: 'Cząstki stałe (liczba):' })
  drawAt({ page: 3, row: 49, col: 1, text: '48.1.' })
  drawAt({ page: 3, row: 49, col: 2, text: 'Współczynnik absorpcji uwzględniający dymienie [m⁻¹]:' })
  drawAt({ page: 3, row: 50, col: 1, text: '48.2.' })
  drawAt({ page: 3, row: 50, col: 2, text: 'Deklarowane maksymalne wartości RDE (w stosownych przypadkach):' })
  drawAt({ page: 3, row: 51, col: 7, text: 'NOx [mg/km]' })
  drawAt({ page: 3, row: 51, col: 9, text: 'Cząstki stałe (liczba) [x/km]' })
  drawAt({ page: 3, row: 52, col: 2, text: 'Całkowity przejazd w badaniu RDE:' })
  drawAt({ page: 3, row: 53, col: 2, text: 'Miejska część przejazdu w badaniu RDE:' })
  drawAt({ page: 4, row: 1, col: 1, text: '49.' })
  drawAt({ page: 4, row: 1, col: 2, text: 'Emisje CO₂ / zużycie paliwa / zużycie energii elektrycznej:' })
  drawAt({ page: 4, row: 2, col: 2, text: '1. Wszystkie zespoły napędowe z wyjątkiem pojazdów hybrydowych z napędem elektrycznym OVC (w stosownych przyp.)' })
  drawAt({ page: 4, row: 3, col: 2, text: 'Wartości WLTP' })
  drawAt({ page: 4, row: 3, col: 4, text: 'Emisje CO₂ [g/km]' })
  drawAt({ page: 4, row: 3, col: 7, text: 'Zużycie paliwa' })
  drawAt({ page: 4, row: 3, col: 10, text: 'Zużycie energii elektrycznej\n[Wh/km]' })
  drawAt({ page: 4, row: 4, col: 3, text: 'benzyna/ON\tgaz: CNG/LPG\tinne' })
  drawAt({ page: 4, row: 4, col: 6.5, text: 'benzyna/ON\tgaz: CNG/LPG\tinne' })
  drawAt({ page: 4, row: 5, col: 2, text: 'Niskie:' })
  drawAt({ page: 4, row: 6, col: 2, text: 'Średnie:' })
  drawAt({ page: 4, row: 7, col: 2, text: 'Wysokie:' })
  drawAt({ page: 4, row: 8, col: 2, text: 'Bardzo wysokie:' })
  drawAt({ page: 4, row: 9, col: 2, text: 'Cykl mieszany:' })
  drawAt({ page: 4, row: 10, col: 2, text: '2. Zasięg pojazdów wyłącznie elektrycznych przy zasilaniu energią elektryczną (w stosownych przypadkach)' })
  drawAt({ page: 4, row: 11, col: 2, text: 'Zasięg przy zasilaniu energią elektryczną [km]:' })
  drawAt({ page: 4, row: 12, col: 2, text: 'Zasięg przy zasilaniu energią elektryczną w mieście [km]:' })
  drawAt({ page: 4, row: 13, col: 2, text: '3. Pojazd wyposażony w ekoinnowację(-e)' })
  drawAt({ page: 4, row: 14, col: 2, text: '3.1. Kod ogólny ekoinnowacji:' })
  drawAt({ page: 4, row: 15, col: 2, text: '3.2. Całkowite ograniczenie emisji CO₂ w wyniku zastosowania ekoinnowacji [g/km]:' })
  drawAt({ page: 4, row: 16, col: 2, text: '3.2.2. Ograniczenie w cyklu WLTP:' })
  drawAt({ page: 4, row: 17, col: 2, text: 'benzyna/ON [g/km]:' })
  drawAt({ page: 4, row: 18, col: 2, text: 'gaz (CNG/LPG) [g/km]:' })
  drawAt({ page: 4, row: 19, col: 2, text: 'inne [g/km]:' })
  drawAt({ page: 4, row: 20, col: 2, text: '4. Pojazdy hybrydowe z napędem elektrycznym OVC (w stosownych przypadkach)' })
  drawAt({ page: 4, row: 21, col: 2, text: 'Wartości WLTP' })
  drawAt({ page: 4, row: 21, col: 4, text: 'Tryb ładowania podtrzymującego' })
  drawAt({ page: 4, row: 22, col: 4, text: 'Emisje CO₂ [g/km]' })
  drawAt({ page: 4, row: 22, col: 7, text: 'Zużycie paliwa' })
  drawAt({ page: 4, row: 22, col: 10, text: 'Zużycie energii elektrycznej\n(EC) [Wh/km]' })
  drawAt({ page: 4, row: 23, col: 3, text: 'benzyna/ON\tgaz: CNG/LPG\tinne' })
  drawAt({ page: 4, row: 23, col: 6.5, text: `benzyna/ON\tgaz: CNG/LPG\tinne\n[/100km]\t[m³/l]/[100 km]\t[kg]/[100 km]` })
  drawAt({ page: 4, row: 24, col: 2, text: 'Niskie:' })
  drawAt({ page: 4, row: 25, col: 2, text: 'Średnie:' })
  drawAt({ page: 4, row: 26, col: 2, text: 'Wysokie:' })
  drawAt({ page: 4, row: 27, col: 2, text: 'Bardzo wysokie:' })
  drawAt({ page: 4, row: 28, col: 2, text: 'Cykl miejski:' })
  drawAt({ page: 4, row: 29, col: 2, text: 'Cykl mieszany:' })
  drawAt({ page: 4, row: 30, col: 2, text: 'Wartości WLTP' })
  drawAt({ page: 4, row: 30, col: 4, text: 'Tryb rozładowania' })
  drawAt({ page: 4, row: 31, col: 4, text: 'Emisje CO₂ [g/km]' })
  drawAt({ page: 4, row: 31, col: 7, text: 'Zużycie paliwa' })
  drawAt({ page: 4, row: 31, col: 10, text: 'Zużycie energii elektrycznej\n(EC) [Wh/km]' })
  drawAt({ page: 4, row: 32, col: 3, text: 'benzyna/ON\tgaz: CNG/LPG\tinne' })
  drawAt({ page: 4, row: 32, col: 6.5, text: `benzyna/ON\tgaz: CNG/LPG\tinne\n[/100km]\t[m³/l]/[100 km]\t[kg]/[100 km]` })
  drawAt({ page: 4, row: 33, col: 2, text: 'Cykl mieszany:' })
  drawAt({ page: 4, row: 34, col: 2, text: 'Wartości ważone, cykl mieszany:' })
  drawAt({ page: 4, row: 35, col: 2, text: '5. Zasięg pojazdów hybrydowych z napędem elektrycznym OVC przy zasilaniu energią elektryczną [km]\n(w stosownych przypadkach)' })
  drawAt({ page: 4, row: 37, col: 2, text: 'Równoważny zasięg przy zasilaniu energią elektryczną (EAER):' })
  drawAt({ page: 4, row: 38, col: 2, text: 'Równoważny zasięg przy zasilaniu energią elektryczną w mieście (EAER city):' })
  drawAt({ page: 4, row: 39, col: 2, text: 'Zasięg przy zasilaniu energią elektryczną (AER):' })
  drawAt({ page: 4, row: 40, col: 2, text: 'Zasięg przy zasilaniu energią elektryczną w mieście (AER city):' })
  drawAt({ page: 4, row: 41, col: 1, text: '50.' })
  drawAt({ page: 4, row: 41, col: 2, text: `Udzielono homologacji typu zgodnie z wymogami projektowymi dotyczącymi przewozu towarów niebezpiecznych
określonymi w regulaminie ONZ nr 105 Europejskiej Komisji Gospodarczej ONZ:`
  })
  drawAt({ page: 4, row: 43, col: 1, text: '51.' })
  drawAt({ page: 4, row: 43, col: 2, text: `W przypadku pojazdów specjalnego przeznaczenia: oznaczenie zgodnie z częścią A pkt 5 załącznika I
do rozporządzenia Parlamentu Europejskiego i Rady (UE) 2018/858:`
  })
  drawAt({ page: 4, row: 45, col: 1, text: '52.' })
  drawAt({ page: 4, row: 45, col: 2, text: 'Uwagi' })
  drawAt({ page: 4, row: 51, col: 1, text: '54.' })
  drawAt({ page: 4, row: 51, col: 2, text: 'Pojazd wyposażony w zaawansowane układy pojazdu:' })
  drawAt({ page: 4, row: 52, col: 1, text: '55.' })
  drawAt({ page: 4, row: 52, col: 2, text: 'Pojazd certyfikowany zgodnie z regulaminem ONZ nr 155:' })
  drawAt({ page: 4, row: 53, col: 1, text: '56.' })
  drawAt({ page: 4, row: 53, col: 2, text: 'Pojazd certyfikowany zgodnie z regulaminem ONZ nr 156:' })

  const pdfBytes = await pdfDoc.save()
  const modelDir = modelDirSwitch(data)
  const filename = `docs/${modelDir}/${zfill(data.ordinalNumber, 3)}_${data.vin}_approval-cerificate.pdf`.replaceAll('?', '-')
  Deno.writeFileSync(filename, pdfBytes)
  log.info(`\n${data.vin}: Świadectwo zgodności utworzone`, ['bold', 'green'])
  log.info(` > ${filename}`, ['gray'])
}

export async function printStatement (data) {
  const pdfDoc = await PDFDocument.create()
  pdfDoc.registerFontkit(fontkit)
  const normal = await pdfDoc.embedFont(Deno.readFileSync(fonts.Skoda.normal))
  const bold = await pdfDoc.embedFont(Deno.readFileSync(fonts.Skoda.bold))
  const fontSize = 12
  const page = pdfDoc.addPage(PageSizes.A4)
  page.setFont(normal)
  page.setFontSize(fontSize)
  const lineHeight = 15
  page.setLineHeight(lineHeight)
  const { width, height } = page.getSize()
  const margin = 39
  try {
    const uint8Array = Deno.readFileSync('images/vgp.png')
    const logoImage = await pdfDoc.embedPng(uint8Array)
    const logoDims = logoImage.scale(0.2)
    page.drawImage(logoImage, {
      x: width/2 - logoDims.width/2,
      y: height - logoDims.height - 20,
      width: logoDims.width,
      height: logoDims.height
    })
  } catch (e) {
    console.error(e)
  }
  page.drawText('OŚWIADCZENIE', {
    x: 230,
    y: height - 120,
    size: 16,
    font: bold
  })
  page.drawText('zawierające dane i informacje o pojeździe', {
    x: 160,
    y: height - 140,
    size: 14,
    font: bold
  })
  page.drawText('niezbędne do rejestracji i ewidencji pojazdów', {
    x: 150,
    y: height - 155,
    size: 14,
    font: bold
  })
  page.drawText(`Oświadczam o dodatkowych, wynikających z obowiązujących na terytorium Rzeczypospolitej
Polskiej przepisów, danych i informacjach o pojeździe niezbędnych do jego rejestracji i ewidencji,
który odpowiada pod każdym względem typowi opisanemu w świadectwie homologacji typu WE
pojazdu/UE pojazdu/krajowym świadectwie homologacji typu pojazdu nr
z dnia:`, {
    x: margin,
    y: height - 200
  })
  page.drawLine({
    start: { x: 83, y: height - 242 },
    end: { x: 410, y: height - 242 },
  })
  page.drawText(data.homologationNumber2ndStage, {
    x: 425,
    y: height - 245,
    font: bold
  })
  page.drawText(data.homologationDate2ndStage, {
    x: 75,
    y: height - 260,
    font: bold
  })
  const homologationDateWidth = bold.widthOfTextAtSize(data.homologationDate2ndStage, fontSize)
  page.drawText(', które zostało uznane przez organ właściwy do uznania tego świadectwa', {
    x: 75 + homologationDateWidth,
    y: height - 260,
  })
  page.drawText('w drodze decyzji nr - z dnia -', {
    x: margin,
    y: height - 275,
  })

  const tableStart = height - 300
  const rowHeight = 20
  const col2 = 70
  const col3 = width - 200
  const col2width = col3 - col2
  const col3width = width - col3 - margin

  const tableContent = [
    ['Lp.', 'Dane i informacje o pojeździe', 'Wyszczególnienie'],
    ['1.', 'Numer VIN albo numer nadwozia, podwozia lub ramy', data.vin],
    ['2.', 'Rodzaj', 'ciężarowy'],
    ['3.', 'Podrodzaj', 'VAN'],
    ['4.', 'Przeznaczenie', 'nie dotyczy'],
    ['5.', 'Rok produkcji', data.productionDate.substring(0, 4)],
    ['6.', 'Masa własna [kg]', `${data.actualWeight2ndStage - 75}`],
    ['7.', 'Dopuszczalna ładowność [kg]', `${data.permissibleMaximumWeight - (data.actualWeight2ndStage - 75)}`],
    ['8.', 'Największy dopuszczalny nacisk osi [kN]', data.permissibleMaximumAxlesWeight.toLocaleString('PL').replace('.', ',')],
    ['9.', 'Dopuszczalna masa całkowita pojazdu [kg]', data.permissibleMaximumWeight],
    ['10.', 'Dopuszczalna masa całkowita zespołu pojazdów [kg]', data.permissibleMaximumWithTrailerWeight],
    ['11.', 'Maksymalna masa całkowita przyczepy z hamulcem [kg]', data.centreAxleTrailerWeight],
    ['12.', 'Inne', `zmiana liczby miejsc z ${data.seatsAmount} na 2`],
    // ['', data.addInfo, 'Numer rejestru Bazy Danych\no Odpadach (nr BDO): 000008136'],
    // ['Poznań', '', data.date]
  ]

  drawTable(margin, tableStart, rowHeight, tableContent, col2, col3)

  function drawTable (margin, tableStart, rowHeight, contents, col2, col3) {
    const size = contents.length;
    [...Array(size).keys()].forEach(i => {
      const lp = i === 0 ? 'Lp.' : `${i}.`
      const th = i === 0 ? 2 : 1
      const op = i === 0 ? 1 : 0.3
      const position = tableStart - rowHeight * i
      page.drawText(lp, { x: margin, y: position })
      page.drawText(contents[i][1], { x: col2, y: position })
      rightAlign(contents[i][2], {
        x: col3,
        y: position,
        containerWidth: col3width,
        font: normal,
        size: fontSize
      })
      if (i < size - 1) {
        page.drawLine({
          start: { x: margin, y: position - rowHeight * 0.3 },
          end: { x: width - margin, y: position - rowHeight * 0.3 },
          thickness: th,
          opacity: op
        })
      }
    })
  }

  function rightAlign (text, { x, y, containerWidth, font, size }) {
    const textWidth = font.widthOfTextAtSize(text, size)
    page.drawText(text, {
      x: x + (containerWidth - textWidth),
      y: y,
      font: font,
      size: size
    })
  }

  page.drawText(data.addInfo, {
    x: col2,
    y: tableStart - rowHeight * 13 + 6,
    maxWidth: col3 - col2,
    size: 9
  })
  page.drawLine({
    start: { x: col3, y: tableStart - rowHeight * 12.3 },
    end: { x: width - margin, y: tableStart - rowHeight * 12.3 },
    thickness: 1,
    opacity: 0.3
  })
  rightAlign('Numer rejestru Bazy Danych', {
    x: col3,
    y: tableStart - rowHeight * 13,
    containerWidth: col3width,
    font: normal,
    size: fontSize
  })
  rightAlign('o Odpadach (nr BDO):', {
    x: col3,
    y: tableStart - rowHeight * 14,
    containerWidth: col3width,
    font: normal,
    size: fontSize
  })
  rightAlign('000008136', {
    x: col3,
    y: tableStart - rowHeight * 15,
    containerWidth: col3width,
    font: normal,
    size: fontSize
  })
  page.drawLine({
    start: { x: margin, y: tableStart - rowHeight * 15.3 },
    end: { x: width - margin, y: tableStart - rowHeight * 15.3 },
    thickness: 1,
    opacity: 0.3
  })
  page.drawText('Poznań', {
    x: margin,
    y: tableStart - rowHeight * 16
  })
  rightAlign(data.date, {
    x: col3,
    y: tableStart - rowHeight * 16,
    containerWidth: col3width,
    font: normal,
    size: fontSize
  })
  rightAlign(data.user, {
    x: col2,
    y: tableStart - rowHeight * 19,
    containerWidth: col2width + col3width,
    font: normal,
    size: 9
  })
  page.drawText(`Nr komisji: ${data.commissionNumber} | Nr dealera: ${data.dealerNumber}`, {
    x: width / 2 - 80,
    y: tableStart - rowHeight * 26,
    size: 9,
    color: grayscale(0.5)
  })

  const pdfBytes = await pdfDoc.save()
  const modelDir = modelDirSwitch(data)
  const filename = `docs/${modelDir}/${zfill(data.ordinalNumber, 3)}_${data.vin}_statement.pdf`.replaceAll('?', '-')
  Deno.writeFileSync(filename, pdfBytes)
  log.info(`\n${data.vin}: Oświadczenie utworzone`, ['bold', 'green'])
  log.info(` > ${filename}`, ['gray'])
}

function modelDirSwitch (data) {
  let model = data.tradeName2ndStage.split(' ')[0].toLowerCase()
  if (model === 'rapid') {
    const year = (new Date()).getFullYear()
    model += year
  }
  return model
}

function zfill (number, size) {
  number = number.toString()
  while (number.length < size) {
    number = '0' + number
  }
  return number
}

export function addToPrintedList (data) {
  const model = data.tradeName2ndStage.split(' ')[0].toLowerCase()
  const filepath = `db/list/${model}.txt`
  const filedata = Deno.readTextFileSync(filepath)
  if (!filedata.includes(data.vin)) {
    const encoder = new TextEncoder()
    const ecoded = encoder.encode('\n' + data.vin)
    Deno.writeFileSync(filepath, ecoded, { append: true })
    log.info(`\n${data.vin}: Numer dodany do listy wydrukowanych dokumentów`, ['bold', 'yellow'])
    log.info(` > ${filepath}`, ['gray'])
  }
}
