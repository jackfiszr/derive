import { join } from "@std/path";
import { CookieJar, wrapFetch } from "jsr:@jd1378/another-cookiejar@^5.0.7";

const configPath = join("services", "drive-config.json");
const payloadPath = join("db", "payload.json");
const dataDir = join("db", "data");

export async function main(vin?: string) {
  const oldProxy = Deno.env.get("HTTPS_PROXY") || "";
  Deno.env.set("HTTPS_PROXY", "");

  vin = vin || Deno.args[0];
  const data = await driveFetch(vin);

  if (data) {
    const dataFile = join(dataDir, `${vin}.json`);
    await Deno.writeTextFile(dataFile, data);
    print(` > save data in ${dataFile}`, "");
  }

  Deno.env.set("HTTPS_PROXY", oldProxy);
}

async function driveFetch(vin: string) {
  print(` > fetch data for ${vin}`);

  const config = JSON.parse(await Deno.readTextFile(configPath));
  const LOGIN_URL = config.LOGIN_URL;
  const DATA_URL = config.DATA_URL;
  const DOCS_URL = config.COC_URL;

  const payload = JSON.parse(await Deno.readTextFile(payloadPath));
  const cookieJar = new CookieJar();
  const fetch = wrapFetch({ cookieJar });

  const loginResponse = await fetch(LOGIN_URL, {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: new URLSearchParams(payload).toString(),
  });

  if (!loginResponse.ok) {
    print(" ! DRIVE LOGIN FAIL\n ! data not saved", "");
    return;
  }

  const docsResponse = await fetch(`${DOCS_URL}${vin}`);
  const docsText = await docsResponse.text();

  if (docsText.includes("Użytkownik niezalogowany")) {
    print(" ! DRIVE LOGIN FAIL\n ! data not saved", "");
    return;
  }

  const docsData = JSON.parse(docsText).data;
  if (docsData.length === 0) {
    print(` ! NO DATA FOR ${vin}\n ! data not saved`, "");
    return;
  }

  const dealerNumber = docsData.at(-1)?.dealerNumber ||
    docsData[0]?.dealerNumber;
  if (!dealerNumber) {
    print(" ! missing dealer number\n ! data not saved", "");
    return;
  }

  print(` > add dealer number ${dealerNumber}`);

  const dataResponse = await fetch(`${DATA_URL}${vin}`);
  const data = JSON.parse(await dataResponse.text());
  const innerData = data.data[0];

  innerData.dealerNumber = dealerNumber;
  innerData.addInfo = "";
  data.data[0] = innerData;

  return JSON.stringify(data, null, 4);
}

function print(text: string, end = "\n") {
  const encoder = new TextEncoder();
  Deno.stdout.writeSync(encoder.encode(text + end));
}

if (import.meta.main) {
  await main();
}
